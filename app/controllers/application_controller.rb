class ApplicationController < ActionController::Base
  protect_from_forgery

  private
  def current_user
    # ||= means if lhs is undefined or falsey evaluate rhs and assign it to lhs
    # Here this means @current_user is only evaluated once.
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user

  def authorize
    redirect_to login_path, alert: "Not authorized" if current_user.nil?
  end
end
