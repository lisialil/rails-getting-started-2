class SessionsController < ApplicationController
  def new
    flash[:referer] = request.referer
  end

  def create
    # this is where the actual authentication happens
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_path = (flash[:referer].nil? ? root_path : flash[:referer])
      redirect_to redirect_path, notice: "Logged in!", status: :see_other
    else
      flash.now.alert = "Email or password is invalid."
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    session[:user_id] = nil
    # alternatively
    # reset_session
    redirect_back_or_to root_path, notice: "Logged out", status: :see_other
  end
end
