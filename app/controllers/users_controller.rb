class UsersController < ApplicationController
  def new
    @user = User.new
    flash[:referer] = request.referer
  end

  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id
      redirect_path = (flash[:referer].nil? ? root_path : flash[:referer])
      redirect_to redirect_path, notice: "Thank you for signing up!", status: :see_other
    else
      render :new, status: :unprocessable_entity
    end
  end

  private
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
