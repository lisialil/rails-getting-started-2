class CommentsController < ApplicationController
   # http_basic_authenticate_with name: "dhh", password: "secret", only: :destroy

  before_action :authorize, only: [:create, :destroy]

  def create
    @article = Article.find(params[:article_id])
    # this takes advantage of the association between article and comment: this automatically links the comment so it belongs to the article
    @comment = @article.comments.create(comment_params)
    redirect_to article_path(@article)
  end

  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    @comment.destroy
    redirect_to article_path(@article), status: 303
  end

  private
  def comment_params
    params.require(:comment).permit(:commenter, :body, :status)
  end
end
